package baci0176MV.validator;

import baci0176MV.controller.DidacticFunction;
import baci0176MV.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeValidatorTest {
    private EmployeeValidator validator;

    @BeforeEach
    void setUp() {
        validator = new EmployeeValidator();
    }

    @Test
    void validEmployeeShouldPassValidation() {
        Employee employee = new Employee(
                "Zoë",
                "Morevna",
                "2871031001100",
                DidacticFunction.ASISTENT,
                2000.0
        );

        assertTrue(validator.isValid(employee));
    }

    @Test
    void invalidCnpEmployeeShouldNotPassValidation() {
        Employee employee = new Employee(
                "Zoë",
                "Morevna",
                "287103100110011",
                DidacticFunction.ASISTENT,
                2000.0
        );

        assertFalse(validator.isValid(employee));
    }

    @Test
    void invalidSalaryEmployeeShouldNotPassValidation() {
        Employee employee = new Employee(
                "Zoë",
                "Morevna",
                "2871031001100",
                DidacticFunction.ASISTENT,
                -1d
        );

        assertFalse(validator.isValid(employee));
    }
}