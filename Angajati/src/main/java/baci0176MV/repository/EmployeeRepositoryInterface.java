package baci0176MV.repository;

import java.util.List;

import baci0176MV.controller.DidacticFunction;
import baci0176MV.model.Employee;

public interface EmployeeRepositoryInterface {
	boolean addEmployee(Employee employee);
	void modifyEmployeeFunction(Employee employee, DidacticFunction newFunction);
	List<Employee> getEmployeeList();
	List<Employee> getEmployeeByCriteria();
	Employee findEmployeeById(int idOldEmployee);
	boolean removeEmployee(int id);
}
